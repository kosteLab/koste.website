import React, { FC } from 'react';
import Header from '../Header';
import Footer from '../Footer';
import Head from "next/head";

const Layout: FC = ({ children }) => {
  return (
    <main>
      <Head>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      {children}
      <Footer />
    </main>
  );
};

export default Layout;