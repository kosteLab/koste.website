import React, {FC} from 'react';
import {Grid, Container} from "@mui/material";
import Menu from '../../Menu';

const Header: FC = () => {
  return (
    <header>
      <Container>
        <Grid container>
          <Grid item lg={12}>
            <Menu />
          </Grid>
        </Grid>
      </Container>
    </header>
  );
};

export default Header;