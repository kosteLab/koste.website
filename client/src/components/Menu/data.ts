export const menu = [
  {
    id: 0,
    name: 'Главная',
    href: '/'
  },
  {
    id: 1,
    name: 'Технологии',
    href: '/tech'
  },
  {
    id: 2,
    name: 'Компоненты',
    href: '/components'
  },
]