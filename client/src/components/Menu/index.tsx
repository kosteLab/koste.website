import React, {FC} from 'react';
import Link from 'next/link';
import {menu} from "./data";

const Menu: FC = () => {
  return (
    <div>
      {menu.map(item => <Link href={item.href} passHref key={item.id}>{item.name}</Link>)}
    </div>
  );
};

export default Menu;