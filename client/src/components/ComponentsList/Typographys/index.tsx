import React, {FC} from 'react';
import {Typography} from "@mui/material";

const Typographys: FC = () => {
  return (
    <div>
      <Typography variant={"h2"}>Типография</Typography>

      <Typography variant={"h1"}>Заголовок h1</Typography>
      <Typography variant={"h2"}>Заголовок h2</Typography>
      <Typography variant={"h3"}>Заголовок h3</Typography>
      <Typography variant={"h4"}>Заголовок h4</Typography>
      <Typography variant={"h5"}>Заголовок h5</Typography>
      <Typography variant={"h6"}>Заголовок h6</Typography>
    </div>
  );
};

export default Typographys;