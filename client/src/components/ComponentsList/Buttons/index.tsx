import React, {FC} from 'react';
import {Button, Typography} from "@mui/material";

const Buttons: FC = () => {
  return (
    <div>
      <Typography variant={"h2"}>Кнопки</Typography>

      <Button variant={"outlined"} color={"primary"}>Outlined</Button>
      <Button variant={"contained"} color={"primary"}>Contained</Button>
      <Button variant={"outlined"} color={"primary"} disabled>Disabled</Button>
      <Button variant={"contained"} color={"primary"} disabled>Disabled</Button>
    </div>
  );
};

export default Buttons;