import React, {FC} from 'react';
import {Typography} from "@mui/material";
import Buttons from './Buttons'
import Typographys from './Typographys'

const ComponentsList:FC = () => {
  return (
    <div>
      <Typography variant={"h1"}>Компоненты</Typography>

      <Buttons />

      <Typographys />
    </div>
  );
};

export default ComponentsList;