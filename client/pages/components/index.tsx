import React from 'react';
import { NextPage } from "next";
import Head from "next/head";
import ComponentsList from '../../src/components/ComponentsList';

const Components: NextPage = () => {
  return (
    <div>
      <Head>
        <title>koste. Компоненты</title>
        <meta name="description" content="koste. Components" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ComponentsList />
    </div>
  );
};

export default Components;