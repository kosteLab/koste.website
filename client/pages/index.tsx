import type { NextPage } from 'next'
import Head from 'next/head'

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>koste. Home page</title>
        <meta name="description" content="koste. Home page" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      Home page
    </div>
  )
}

export default Home
